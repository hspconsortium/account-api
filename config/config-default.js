module.exports = {
  newsletter: {
    newsletter1Id: '52c1cad342',
    newsletter2Id: 'e0a0af33ae',
    mailchimpUrl: 'https://us16.api.mailchimp.com/3.0',
    mailchimpApiKey: 'secret'
  },
  firebase: {
    project: 'hspc-tst',
    firebaseCliToken: 'secret',
    private_key: 'secret'
  },
  mail: {
    password: 'secret'
  }
};