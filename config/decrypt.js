var crypto;

// import crypto
try {
  crypto = require('crypto');
} catch (err) {
  console.warn("Crypto is not enabled in this node build, properties will not be decrypted.");
}

// retrieve password from env variable
var password = process.env.JASYPT_ENCRYPTOR_PASSWORD;

if (!password) {
  console.warn("There is no decryption password set for this environment, any encrypted properties will not be decrypted.");
}

var algorithm = 'aes-256-ctr';


function decrypt(text) {
  var decipher = crypto.createDecipher(algorithm, password);
  var dec = decipher.update(text, 'hex', 'utf8');
  dec += decipher.final('utf8');
  return dec;
}

function decryptProperties(properties) {
  if (!password || !crypto) {
    return properties;
  }
  eachRecursive(properties);
  return properties;
}

function decryptIfNeeded(text) {
  if (text && text.substr(0, 4) === 'ENC(' && text.endsWith(')')) {
    var encryptedPart = text.substring(4, text.length - 1);
    var decryptedText = decrypt(encryptedPart);
    return decryptedText;
  }

  return text;
}

function eachRecursive(obj) {
  for (var k in obj) {
    if (typeof obj[k] == "object" && obj[k] !== null)
      eachRecursive(obj[k]);
    else if (obj.hasOwnProperty(k)) {
      obj[k] = decryptIfNeeded(obj[k]);
    }
  }
}

module.exports = {
  decryptString: decrypt,
  decryptProperties: decryptProperties
};