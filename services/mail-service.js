var nodemailer = require('nodemailer');
var config = require('../config/config');

// create reusable transporter object using the default SMTP transport
var transporter = nodemailer.createTransport({
  host: 'email-smtp.us-west-2.amazonaws.com',
  port: 465,
  secure: true, // secure:true for port 465, secure:false for port 587
  auth: {
    user: 'AKIAJMCLR56W3IX636FA',
    pass: config.mail.password
  }
});

function sendMail(recipient, subject, contents){
  //setup email data with unicode symbols
  var mailOptions = {
    from: '"HSPC DEV" <mike@interopion.com>', // sender address
    to: recipient, // list of receivers
    subject: subject, // Subject line
    text: contents // plain text body
  };

  transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      return console.log(error);
    }
    console.log('Message %s sent: %s', info.messageId, info.response);
  });
}

module.exports = {
  sendMail: sendMail
};
