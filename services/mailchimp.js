var config = require('../config/config');
var Client = require('node-rest-client-promise').Client;
var md5 = require('md5');

var mailchimp = {};

mailchimp.checkSubscription = function(email, listId) {
  var client = new Client();
  var mailchimpUrl = config.newsletter.mailchimpUrl + '/lists/' + listId + '/members/' + md5(email);
  var args = {
    data: {},
    headers: {
      "Content-Type": "application/json",
      "Authorization": "Basic " + base64Encode('dev:' + config.newsletter.mailchimpApiKey)
    }
  }
  return client.getPromise(mailchimpUrl, args)
}

mailchimp.subscribeToList = function (email, firstName, lastName, listId) {
  var client = new Client();
  var mailchimpUrl = config.newsletter.mailchimpUrl + '/lists/' + listId + '/members';
  var args = {
    data: {
      email_address: email,
      status: 'subscribed',
      merge_fields: {
        FNAME: firstName,
        LNAME: lastName
      }
    },
    headers: {
      "Content-Type": "application/json",
      "Authorization": "Basic " + base64Encode('dev:' + config.newsletter.mailchimpApiKey)
    }
  };
  return client.postPromise(mailchimpUrl, args);
};

mailchimp.resubscribeToList = function (email, firstName, lastName, listId) {
  var client = new Client();
  var mailchimpUrl = config.newsletter.mailchimpUrl + '/lists/' + listId + '/members/'+md5(email);
  var args = {
    data: {
      email_address: email,
      status: 'subscribed',
      merge_fields: {
        FNAME: firstName,
        LNAME: lastName
      }
    },
    headers: {
      "Content-Type": "application/json",
      "Authorization": "Basic " + base64Encode('dev:' + config.newsletter.mailchimpApiKey)
    }
  };
  return client.patchPromise(mailchimpUrl, args);
};

mailchimp.unsubscribeToList = function (email, listId) {
  var client = new Client();
  var mailchimpUrl = config.newsletter.mailchimpUrl + '/lists/' + listId + '/members/'+md5(email);
  var args = {
    data: {
      status: "unsubscribed"
    },
    headers: {
      "Content-Type": "application/json",
      "Authorization": "Basic " + base64Encode('dev:' + config.newsletter.mailchimpApiKey)
    }
  };
  // return client.deletePromise(mailchimpUrl, args)
  return client.patchPromise(mailchimpUrl, args);
};

mailchimp.getIdForNewsletter = function (newsletter) {
  switch (newsletter) {
    case 'newsletter1':
      return config.newsletter.newsletter1Id;
    case 'newsletter2':
      return config.newsletter.newsletter2Id;
  }
  return null;
};

module.exports = mailchimp;

////
// Private Methods

function base64Encode(toEncode) {
  return new Buffer(toEncode).toString('base64');
}